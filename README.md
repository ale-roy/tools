# Tools

__Tools__ is a personal project which give me an easy access to my everyday carry tools. It gathers my configuration files such as *shell_config* and *vim_config* files and *vim_dir* directory. Thanks to the magic behing __Git__, I'm also able to manage easily my programs into a *bin* directory so I can easily `push/pull` modifications at any time.

## Usage

In order to use __Tools__ features, you have to propagate the project's files. This can be done by using these commands in your Unix shell.

```
cd ~
git clone git@gitlab.com:ale-roy/tools.git
cd tools
./propagate
```

The *shell_config* has to be sourced from your own shell configuration file. By doing so you are able to keep your own configurations and update only what needs to be update or propagated.

## Backup

Before writing changes to the configurations files, the _propagate_ script creates backup files from previous versions. You will find these files in your `$HOME` directory under the name `.<config_file>_backup`. You can undo the previous propagation at any time by runing these commands:

```
cd ~
mv .shell_config_backup .shell_config
mv .vimrc_backup .vimrc
```

## License

I choose the MIT license. It can be found in the _LICENSE.md_ file (root project directory).
